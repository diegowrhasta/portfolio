# Portfolio

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 17.3.7.

## Introduction

Codebase for personal portfolio under domain [my-portfolio](https://my-portfolio.dsbalderrama.top).

## Description

This is a simple Angular SPA application that makes use of SASS (Scss) for style sheets, and with clear defined variables for main color, primary and secondary. Under the styles there's also media queries applied for a responsive layout (mobile).

Everything has been put behind components so that code is less repeated and extensions are easier to it. The only outside dependency the portfolio has is to a json database to which the contact messages are sent to.

As just some fun extra pieces, two more primary colors were implemented and they can be switched by clicking the palette sticky icon on the right hand side, thanks to a good usage of variables this implementation was made far easier.

## Final thoughts

A simple portfolio page in order to showcase an overview of my skills, services, trajectory and principles.
