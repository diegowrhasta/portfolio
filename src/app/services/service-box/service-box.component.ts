import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-service-box',
  templateUrl: './service-box.component.html',
  styleUrl: './service-box.component.scss'
})
export class ServiceBoxComponent {
  @Input()
  serviceName?: string;

  @Input()
  description?: string;
}
