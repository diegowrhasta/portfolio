import { Component } from '@angular/core';

export type ServiceEntry = {
  serviceName: string;
  description: string;
};

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrl: './services.component.scss',
})
export class ServicesComponent {
  spectrumTitle: string = 'Skill Spectrum';
  spectrumDescription: string = `The 'Full Stack' term can easily end up as nebulous 
  as it can be, and that's because each team, each project, varies on the amount of tasks 
  that a developer will take. The diagram up above details the types of roles we might 
  encounter on a software development organization, ranging from people that are always 
  next to a server rack, touching electronic components, all the way to the user, 
  may it by be presenting software to him or just listening to his requirements. My personal 
  skillset is focused on the areas that go from DevOps related tasks (a bit of IaC), all the way 
  to user interactions (presentation of new products or features, meetings with users and execs plus coordination of
  teams internally). I certainly am more focused and skilled (hence the depth extends 
  further) when it comes to the development process itself. Automation of tasks, establishment 
  of workflows, and the actual work of designing, prototyping, fixing and deploying solutions 
  for requirements that come from the product's user base. Whilst also delving into 
  interactions with Designers through the usage of tools such as Figma, Zeplin, and 
  wireframing things myself with tools such as Balsamiq Mockups`;

  serviceEntries: ServiceEntry[] = [
    {
      serviceName: 'Frontend Development',
      description: `May it be for a new solution or maintenance of an already existing 
      solution I can be there in order to understand requirements and turn them into 
      working software, the degree of polish that can be achieved will always depend on 
      the amount of resources available, yet through work with Design Systems, Atomic 
      Design, and modern frontend-web-development I always aim at bringing the best 
      experiences to the users. (Angular, React.js)`,
    },
    {
      serviceName: 'Backend Development',
      description: `May it be for a new solution or maintenance of already existing 
      implementation, I can always be there in order to deal with the core logic of the 
      business rules, whilst the backend does not have much of a visual impact, I can 
      assure you that my work will always follow good practices, modern solutions, 
      performant implementations, scalable services and with a high degree of integration 
      with CI/CD processes. (ASP.NET, Spring, Laravel)`,
    },
    {
      serviceName: 'Software Design',
      description: `If there's a need to create an architecture that's brand new or 
      revise an already existing one, I'll be sure to leverage all my knowledge 
      in order to figure out an implementation that will come from a simple idea 
      and then be turned into reality. The architecture that will be used is one
      that is aimed at being the best approximation at a good fit to the current 
      problem and current possibilities that are available.
      (Azure, In-House)`,
    },
    {
      serviceName: 'IT Consulting',
      description: `As a full-stack engineer I do not have a set allegiance to any 
      specific area of software development, whilst I do have a higher affinity for backend 
      and architecturing services, I can definitely fit in wherever it's needed, I'm 
      always open to learn new things, and always keep up with new trends, practices and 
      technologies that get brought up. (Frontend, Backend, SDLC Processes and Architecting)`,
    },
  ];
}
