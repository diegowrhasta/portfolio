import { Component } from '@angular/core';

export type EducationEntry = {
  year: number;
  place: string;
  description: string;
};

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrl: './education.component.scss',
})
export class EducationComponent {
  educationEntries: EducationEntry[] = [
    {
      year: 2015,
      place: 'High School',
      description: `Graduated from 'Domingo Savio' Institue on La Paz, Bolivia. 
      Whilst finishing my 12 years of school I also got an associate's degree on 
      Hardware and Networking from 'Atenea' Institute on La Paz, plus an associate's 
      degree on 'Proficiency in English' from 'First Class Institute' on La Paz, and 
      I also went through the 'Goethe Institut's German Language program all the way 
      to the C1 Level.`,
    },
    {
      year: 2019,
      place: 'Internship',
      description: `Before graduating I was accepted as an intern for a Bank entity 
      (Banco Bisa) under the IT Operations Branch, was in charge of research and support 
      on web and mobile applications. The highest achievement that I got there was 
      to lead a presentation for the CTO of the bank in regards to the outlook of 
      the current mobile application in comparison to other bolivian banks and financial 
      entities.`,
    },
    {
      year: 2020,
      place: 'University',
      description: `Graduated with a 'Systems Engineering' degree from 
      'Universidad Católica Boliviana \"San Pablo\", on La Paz Bolivia. Also took 
      part in the Computer Science Chapter of the IEEE branch on the same university 
      as a vice-president. And was part of some efforts alongside the City Hall of 
      La Paz for mobile applications that helped in the city's operations. (Aventon)`,
    },
    {
      year: 2020,
      place: 'Cirrus IT',
      description: `I graduated by doing "Supervised Work", which 
      consisted of developing a system for a real-world company and that be 
      validated as my culminating assignment. The company in which I did this project, 
      which was a software contracting company, then hired me and I started working 
      actively with financial and banking institutions developing web systems by 
      doing full-stack work solving different problems such as digital signing, customer 
      service platforms, with solutions ranging from microservices, to monolithic 
      APIs and powered through Docker in all their instances.`,
    },
    {
      year: 2021,
      place: 'Jalasoft',
      description: `After an opportunity presented itself in which I got a full- 
      scholarhip for a Bootcamp that Jalasoft sponsored I managed to finish the bootcamp 
      and got hired then after to start working as a Full-Stack developer with React and 
      C# experience. I worked with React, Blazor (C#), ASP.NET, Azure, Docker, MongoDB and 
      Angular for many web solutions across different businesses that offered a wide 
      range of solutions.`,
    },
    {
      year: 2023,
      place: 'Unosquare',
      description: `After being scouted by a recruiter and after passing a technical 
      assesment I got hired for an outsourcing company under a Full-Stack .NET developer role. 
      In this new path I managed to become a core member on the team, specifically 
      for implementation of best practices, modern patterns and solutions for web 
      applications that served SaaS solutions. It was in under my role in this company 
      that I managed to work alongside Design Systems and advanced Agile workflows 
      for large development teams. Managed to be one of the few members that took 
      part in meetings with executives and high ranking technical roles in order 
      to implement new solutions and extensions to already existing architectures and 
      implementations in order to modernize them and better both performance and the 
      overall approach in bringing value to customers.`,
    },
    {
      year: 2024,
      place: 'CodeRoad (prev. Mojix)',
      description: `After conversations with a scout, an offer was extended to me 
      in order to become a contractor under Mojix, a software company based on USA, FL.
      The job involved the .NET stack, and the biggest challenge that I faced in 
      here was a migration from on-premises solutions to the cloud (Azure), alongside 
      this, many projects had to be migrated from NET Framework to NET 8, within these 
      migrations a great deal of work was done in order to work with a high-troughput 
      application that served more than 2 Million Transactions a month, so caching, 
      scaling (Kubernetes) and performant code was a big focus`,
    },
  ];
}
