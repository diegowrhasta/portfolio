import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-timeline-item',
  templateUrl: './timeline-item.component.html',
  styleUrl: './timeline-item.component.scss'
})
export class TimelineItemComponent {
  @Input()
  year?: number;
  
  @Input()
  place?: string;
  
  @Input()
  description?: string;

  @Input()
  last?: boolean;

  @Input()
  even?: boolean;
}
