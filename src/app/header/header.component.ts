import {
  Component,
  ElementRef,
  EventEmitter,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { debounceTime, fromEvent, takeUntil, tap } from 'rxjs';

export type NavbarOption = {
  id: number;
  ref: string;
  text: string;
};

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.scss',
})
export class HeaderComponent implements OnInit, OnDestroy {
  @ViewChild('navBar')
  navbar?: ElementRef;

  @ViewChild('menuIcon')
  menuIcon?: ElementRef;

  onDestroy$ = new EventEmitter<boolean>();

  activeMenuItem = 0;

  navbarOptions: NavbarOption[] = [
    {
      id: 0,
      ref: '#home',
      text: 'Home',
    },
    {
      id: 1,
      ref: '#education',
      text: 'Education',
    },
    {
      id: 2,
      ref: '#services',
      text: 'Services',
    },
    // {
    //   id: 3,
    //   ref: '#testimonials',
    //   text: 'Testimonials',
    // },
    {
      id: 3,
      ref: '#projects',
      text: 'Projects',
    },
    {
      id: 4,
      ref: '#contact',
      text: 'Contact',
    },
  ];

  ngOnInit(): void {
    const sections = document.querySelectorAll('section');

    fromEvent(window, 'scroll')
      .pipe(
        takeUntil(this.onDestroy$),
        debounceTime(150),
        tap((_) => {
          sections.forEach((sec) => {
            const top = window.scrollY;
            const offset = sec.offsetTop - 150;
            const height = sec.offsetHeight;
            const id = sec.getAttribute('data-id');

            if (top >= offset && top < offset + height) {
              this.activeMenuItem = +(id ?? '');
            }
          });
        })
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.onDestroy$.emit(true);
  }

  isActive(index: number): boolean {
    return index === this.activeMenuItem;
  }

  onMenuClick(): void {
    this.menuIcon?.nativeElement.classList.toggle('bx-x');
    this.navbar?.nativeElement.classList.toggle('active');
  }

  onMenuItemClick(index: number): void {
    this.activeMenuItem = index;
  }
}
