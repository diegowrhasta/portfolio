import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { EducationComponent } from './education/education.component';
import { ServicesComponent } from './services/services.component';
import { TestimonialsComponent } from './testimonials/testimonials.component';
import { ContactComponent } from './contact/contact.component';
import { FooterComponent } from './footer/footer.component';
import { TimelineItemComponent } from './education/timeline-item/timeline-item.component';
import { ServiceBoxComponent } from './services/service-box/service-box.component';
import { TestimonialItemComponent } from './testimonials/testimonial-item/testimonial-item.component';
import { SocialIconsComponent } from './shared/social-icons/social-icons.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormErrorComponent } from './contact/form-error/form-error.component';
import { StickyButtonComponent } from './sticky-button/sticky-button.component';
import { HttpClientModule } from '@angular/common/http';
import { provideAnimations } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { ProjectsComponent } from './projects/projects.component';
import { ProjectItemComponent } from './projects/project-item/project-item.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    EducationComponent,
    ServicesComponent,
    TestimonialsComponent,
    ContactComponent,
    FooterComponent,
    TimelineItemComponent,
    ServiceBoxComponent,
    TestimonialItemComponent,
    SocialIconsComponent,
    FormErrorComponent,
    StickyButtonComponent,
    ProjectsComponent,
    ProjectItemComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    ToastrModule.forRoot(),
  ],
  providers: [provideAnimations()],
  bootstrap: [AppComponent],
})
export class AppModule {}
