import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { validatePhoneNumber } from './validators';
import { ContactService } from '../app-services/contact.service';
import { tap } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

export type FormModel = {
  fullName: string;
  email: string;
  phoneNumber: number;
  subject: string;
  message: string;
};

export type ControlState = {
  controlName: string;
  showError: boolean;
  errorMessage: string;
};

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrl: './contact.component.scss',
})
export class ContactComponent {
  form: FormGroup;
  submitted = false;

  constructor(
    private fb: FormBuilder,
    private contactService: ContactService,
    private toast: ToastrService
  ) {
    this.form = this.fb.group({
      fullName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phoneNumber: ['', [Validators.required, validatePhoneNumber]],
      subject: ['', Validators.required],
      message: ['', Validators.required],
    });
  }

  sendMessage(): void {
    if (!this.form.valid) {
      this.submitted = true;
      return;
    }

    const structure = this.form.value;

    this.contactService
      .postContactMessage(structure)
      .pipe(
        tap(() => {
          this.form.reset();
          this.toast.success(
            'Thank you, I will be reading your message as soon as I can!',
            'Message Sent',
            { timeOut: 2500 }
          );
        })
      )
      .subscribe();
  }

  computeControlState(
    controlName: string,
    errorMessage: string
  ): ControlState | null {
    const showError =
      (this.form.get(controlName)?.touched &&
        this.form.get(controlName)?.invalid) ||
      (this.submitted && this.form.get(controlName)?.invalid);

    if (!showError) {
      return null;
    }

    return {
      controlName: controlName,
      showError: showError,
      errorMessage: errorMessage,
    };
  }
}
