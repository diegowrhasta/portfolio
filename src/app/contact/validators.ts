import { AbstractControl, ValidationErrors } from '@angular/forms';

export function validatePhoneNumber(
  control: AbstractControl
): ValidationErrors | null {
  const value = control.value as string;
  // Regex to check always starts with +, accepts a possible space or not after and then 
  // for 6 to 14 digits that can be spaced out (with dashes too).
  const phoneNumberRegex = /^\+(?:[0-9] ?){6,14}[0-9]$/;

  if (!phoneNumberRegex.test(value)) {
    return { notPhoneNumber: true };
  }

  return null;
}
