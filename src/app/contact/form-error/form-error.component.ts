import { Component, Input } from '@angular/core';
import { ControlState } from '../contact.component';

@Component({
  selector: 'app-form-error',
  templateUrl: './form-error.component.html',
  styleUrl: './form-error.component.scss',
})
export class FormErrorComponent {
  @Input()
  controlState?: ControlState | null;
}
