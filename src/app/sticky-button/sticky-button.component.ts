import { Component } from '@angular/core';

export type ThemeEntry = {
  theme: string;
  index: number;
};

export const THEMES = class {
  static readonly ORANGE = '';
  static readonly WINE = 'wine-theme';
  static readonly SKY = 'sky-theme';

  static readonly LIST: ThemeEntry[] = [
    { theme: this.ORANGE, index: 0 },
    { theme: this.WINE, index: 1 },
    { theme: this.SKY, index: 2 },
  ];
};

@Component({
  selector: 'app-sticky-button',
  templateUrl: './sticky-button.component.html',
  styleUrl: './sticky-button.component.scss',
})
export class StickyButtonComponent {
  private themeState: ThemeEntry;

  constructor() {
    this.themeState = THEMES.LIST[0];
  }

  onClick(): void {
    this.transition();
  }

  private transition(): void {
    const currentTheme = this.themeState;
    const isMainTheme = currentTheme.index === 0;
    const isLastTheme = currentTheme.index === THEMES.LIST.length - 1;

    if (!isMainTheme) {
      document.body.classList.toggle(currentTheme.theme);
    }

    const nextIndex = isLastTheme ? 0 : currentTheme.index + 1;
    this.themeState = THEMES.LIST[nextIndex];

    document.body.classList.toggle(this.themeState.theme);
  }
}
