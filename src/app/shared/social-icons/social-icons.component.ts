import { Component } from '@angular/core';

export type IconEntry = {
  ref: string;
  iconName: string;
};

@Component({
  selector: 'app-social-icons',
  templateUrl: './social-icons.component.html',
  styleUrl: './social-icons.component.scss',
})
export class SocialIconsComponent {
  iconEntries: IconEntry[] = [
    {
      ref: 'https://www.linkedin.com/in/diego-samuel-balderrama-quino-955a67149/',
      iconName: 'bxl-linkedin',
    },
    {
      ref: 'https://github.com/diegowrhasta',
      iconName: 'bxl-github',
    },
    {
      ref: 'https://gitlab.com/diegowrhasta',
      iconName: 'bxl-gitlab',
    },
    {
      ref: 'https://drive.google.com/drive/folders/1UT-aGj4XZiCvX7J05OwBvX77qsmg6VLO?usp=sharing',
      iconName: 'bx-file',
    },
  ];
}
