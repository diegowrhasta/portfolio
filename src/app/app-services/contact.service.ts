import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ContactService {
  constructor(private httpClient: HttpClient) {}

  postContactMessage(contactMessage: Record<string, unknown>): Observable<any> {
    return this.httpClient
      .post(
        'https://portfolio-9a9f5-default-rtdb.firebaseio.com/message.json',
        contactMessage
      )
      .pipe(
        tap((value) => {
          console.log('posted to backend', value);
        })
      );
  }
}
