import { Component } from '@angular/core';

export type ProjectEntry = {
  title: string;
  image: string;
  summary: string;
  techonologies: string;
  demoUrl?: string;
  sourceUrlOne: string;
  sourceCodeLabelOne: string;
  sourceUrlTwo?: string;
  sourceCodeLabelTwo?: string;
};

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrl: './projects.component.scss',
})
export class ProjectsComponent {
  projects: ProjectEntry[] = [
    {
      title: 'KakeiBro',
      image: '/assets/wip-logo.jpg',
      summary: `(WORK IN PROGRESS) Full Stack application that helps with maganement of 
        personal finances. This a full-blown project made under an MIT license hosted on 
        a GitHub Public Organization. Everything has been built across the multi-disciplinary 
        endavor of software development, documentation for everything is powered through 
        Antora docs. Code hygiene enforced through Git Hooks, CI/CD pipelines, Automated Formatting and Linting. 
        Document data model through Redis OM, Domain Driven Design, a Modular Monolith (Modulith) at the 
        .NET backend, and a Single-Page Application powered through React.`,
      techonologies: `ASP.NET Core (C#), React (Typescript), Redis, GCloud Artifact Run, Firebase Hosting, GitHub 
        Pages, Antora (Asciidoc), Github Actions, Bash, Cake, Husky, Docker, Docker Compose, ESLint, Prettier, Google OAuth, 
        Apache Graphs, Penpot, Vitest, xUnit, Excalidraw, Draw.io, Git`,
      sourceUrlOne: 'https://github.com/orgs/KakeiBro/repositories',
      sourceCodeLabelOne: 'Project Repositories',
    },
    {
      title: 'Toothtrack',
      image: '/assets/toothtrack-preview.png',
      summary: `Full Stack application developed for a private dentist clinic. 
        With features such as management of patients, medical histories, notifications 
        through WhatsApp and Email, all of this on top of an appointment scheduling module. 
        Built a Component Library on the frontend and a Restful API on the backend 
        with Vertical Slice Architecture. The application has a desktop mode and 
        a web mode, being the web mode one with different services brought together 
        in order to be deployed in its entirety and the desktop mode leveraging 
        a fully orchestrated environment in order to get it up and running with 
        a double click of a desktop icon.`,
      techonologies: `ASP.NET Core, Angular, Mongo DB, Sass, Firebase, GCloud Artifact Registry 
        and Cloud Run, Docker, Docker Compose, Electron, Whatsapp API, Email`,
      demoUrl: 'https://toothtrack.dsbalderrama.top',
      sourceUrlOne: 'https://gitlab.com/diegowrhasta/toothtrack-web',
      sourceCodeLabelOne: 'Frontend Code',
      sourceUrlTwo: 'https://gitlab.com/diegowrhasta/toothtrack-services',
      sourceCodeLabelTwo: 'Backend Code',
    },
    {
      title: 'Eshop Microservices',
      image: '/assets/eshop-microservices-preview.png',
      summary: `Full Stack application developed alongside Mehmet's Microservices 
        Course (Udemy). A full detailed description of what it has sits on the README 
        page on the github repo, but in short this is a full stack orchestrated architecture 
        that makes use of the latest .NET 8 features, uses best practices, tons of libraries, 
        and tons of patterns, a great deal of research, and knowledge around architectural 
        patterns, complexity, error handling, rate limiting, resilience, traceability, 
        polyglot services are used, and they result in a prototype for an e-shop storefront.`,
      techonologies: `ASP.NET Core, Razor Pages, Bootstrap, RabbitMQ, Yarp, MediatR, 
      Microsoft SQL, SQLite, PostgreSQL, Docker, Docker Compose,`,
      sourceUrlOne: 'https://github.com/diegowrhasta/EShopMicroservices',
      sourceCodeLabelOne: 'Source Code',
    },
    {
      title: 'The Blog',
      image: '/assets/the-blog-preview.png',
      summary: `Web Application built from the ground up by porting a Figma design 
      (From the Community) into a React Web Application. Support for light, dark 
      modes, 3 responsive screen sizes (Phone, IPad, Desktop). Made use of Zustand 
      for a global store and easier management of state. Due to the leverage of 
      functional testing (vitest), alongside testing-library, the application 
      has accesibility and navigation (keyboard) in mind. It has also a 100% coverage 
      of all code, meaning that edge cases and requirements have been refined to the 
      fullest extent.`,
      techonologies: `React, Typescript, Javascript, Vitest, Testing Library`,
      demoUrl: 'https://the-blog.dsbalderrama.top ',
      sourceUrlOne: 'https://github.com/diegowrhasta/react-blog',
      sourceCodeLabelOne: 'Source Code',
    },
  ];
}
