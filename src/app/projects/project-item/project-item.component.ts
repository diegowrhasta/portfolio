import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-project-item',
  templateUrl: './project-item.component.html',
  styleUrl: './project-item.component.scss',
})
export class ProjectItemComponent {
  @Input()
  title?: string;

  @Input()
  image?: string;

  @Input()
  summary?: string;

  @Input()
  technologies?: string;

  @Input()
  demoUrl?: string;

  @Input()
  sourceUrlOne?: string;

  @Input()
  sourceOneLabel?: string = 'Source Code 1';

  @Input()
  sourceUrlTwo?: string;
  
  @Input()
  sourceTwoLabel?: string = 'Source Code 2';
}
