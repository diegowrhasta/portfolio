import { Component } from '@angular/core';

export type TestimonialEntry = {
  imageSrc: string;
  name: string;
  description: string;
  boxUrl: string;
};

@Component({
  selector: 'app-testimonials',
  templateUrl: './testimonials.component.html',
  styleUrl: './testimonials.component.scss',
})
export class TestimonialsComponent {
  testimonialEntries: TestimonialEntry[] = [
    {
      imageSrc: '/assets/face-one.jpg',
      boxUrl:
        'https://www.linkedin.com/in/diego-samuel-balderrama-quino-955a67149/',
      description: `Lorem ipsum dolor sit amet consectetur adipisicing elit.
    Aliquid accusantium maxime nesciunt doloremque impedit
    corporis nihil a nulla perferendis. Soluta porro voluptatem,
    ipsa hic dolorem similique laboriosam! Consequatur, vero ex!`,
      name: 'Marilyn',
    },
    {
      imageSrc: '/assets/face-two.jpg',
      boxUrl:
        'https://www.linkedin.com/in/diego-samuel-balderrama-quino-955a67149/',
      description: `Lorem ipsum dolor sit amet consectetur adipisicing elit.
    Aliquid accusantium maxime nesciunt doloremque impedit
    corporis nihil a nulla perferendis. Soluta porro voluptatem,
    ipsa hic dolorem similique laboriosam! Consequatur, vero ex!`,
      name: 'Julia',
    },
    {
      imageSrc: '/assets/face-three.jpg',
      boxUrl:
        'https://www.linkedin.com/in/diego-samuel-balderrama-quino-955a67149/',
      description: `Lorem ipsum dolor sit amet consectetur adipisicing elit.
    Aliquid accusantium maxime nesciunt doloremque impedit
    corporis nihil a nulla perferendis. Soluta porro voluptatem,
    ipsa hic dolorem similique laboriosam! Consequatur, vero ex!`,
      name: 'Marilyn',
    },
  ];
}
