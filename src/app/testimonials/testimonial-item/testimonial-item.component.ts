import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-testimonial-item',
  templateUrl: './testimonial-item.component.html',
  styleUrl: './testimonial-item.component.scss',
})
export class TestimonialItemComponent {
  @Input()
  imageSrc?: string;

  @Input()
  name?: string;

  @Input()
  description?: string;

  @Input()
  boxUrl?: string;

  onClick(): void {
    window.open(this.boxUrl, '_blank');
  }
}
